FROM golang:alpine3.18

ARG LINTER_VERSION

RUN wget -O- -nv https://raw.githubusercontent.com/golangci/golangci-lint/${LINTER_VERSION}/install.sh | sh -s
